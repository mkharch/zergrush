
* DONE: Think of a better title

* DONE: Add vertical ticks to the rate graph

* DONE: Display a warning whent the demo reaches maximum capacity

* DONE: Add a hovering highlight to instances

* DONE: Detect lost connection

* DONE: Detect spawning/destruction errors

* DONE: Limit the number of people who use the demo

* DONE: Add vertical lines on the left and right

* DONE: Add hovering hightlight to buttons

* DONE: Add an animation when an instance goes away

* DONE: On click retrieve a page from the instance and display it

* DONE: Destroy 25% instances every minute

* POSTPONED: Enable faster gator restart (SO\_REUSEADDR)

* DONE: Debug segfault in gator

* Add a row of arrows to the bottom of the page

* Write a text describing a demo

