-module(rush_central).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

-export([add_web_socket/1,remove_web_socket/1]).
-export([count_web_sockets/0]).
-export([announce/1]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(st, {socks}).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

add_web_socket(Ws) ->
	gen_server:call(?SERVER, {add_web_socket,Ws}).

remove_web_socket(Ws) ->
	gen_server:call(?SERVER, {remove_web_socket,Ws}).

count_web_sockets() ->
	gen_server:call(?SERVER, count_web_sockets).

announce(Text) ->
	gen_server:call(?SERVER, {announce,Text}).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
    {ok,#st{socks =[]}}.

handle_call({add_web_socket,Ws}, _From, #st{socks =Socks} =St) ->
	io:format("~w open connection(s)\n", [length(Socks) +1]),
    {reply,ok,St#st{socks =[Ws|Socks]}};

handle_call({remove_web_socket,Ws}, _From, #st{socks =Socks} =St) ->
	io:format("~w connection(s) left\n", [length(Socks) -1]),
	{reply,ok,St#st{socks =lists:delete(Ws, Socks)}};

handle_call(count_web_sockets, _From, #st{socks =Socks} =St) ->
	{reply,length(Socks),St};

handle_call({announce,Text}, _From, #st{socks =Socks} =St) ->
	lists:foreach(fun(Ws) ->
		Ws ! {announcement,Text}
	end, Socks),
	{reply,ok,St}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

