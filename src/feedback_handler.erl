-module(feedback_handler).
-behaviour(cowboy_websocket_handler).

-export([init/3]).
-export([websocket_init/3,websocket_handle/3,
    websocket_info/3,websocket_terminate/3]).

init({tcp,http}, _Req, _Opts) ->
    {upgrade,protocol,cowboy_websocket}.

websocket_init(_TransName, Req, _Opts) ->
	{{Addr,_},_} = cowboy_req:peer(Req),
	io:format("A new connection from ~s~n", [inet_parse:ntoa(Addr)]),
	rush_central:add_web_socket(self()),
	Greeting = spawning_pool:make_greeting(),
	self() ! {announcement,Greeting},
	{ok,Req,[]}.

%%websocket_handle({text,_Text}, Req,St) ->
%%	...
%%	{reply,{text,Text},Req,St};
websocket_handle(_Data, Req, St) ->
    {ok,Req,St}.

websocket_info({announcement,Text}, Req, St) ->
	{reply,{text,Text},Req,St};
websocket_info(_Info, Req, St) ->
    {ok,Req,St}.

websocket_terminate(_Reason, _Req, _St) ->
	rush_central:remove_web_socket(self()),
    ok.

%%EOF
