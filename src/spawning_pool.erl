-module(spawning_pool).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

-export([start_spawn/1,start_destroy/1]).
-export([make_greeting/0]).
-export([name_to_addr/1]).

-export([cleanup_stale_rushlings/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-define(STATS_FILE, "zr_stats").
-define(STATS_UPDATE, 5000).

-define(HALF_LIFE_UPDATE, 5000).
-define(HALF_LIFE_BASE_PROB, 0.01).
-define(HALF_LIFE_MAX_PROB, 0.33).
-define(HALF_LIFE_CUT, 20).
-define(HALF_LIFE_PART, 4).

-define(MAX_CAPACITY, 128).		%% sync with ADDR_BYTE

-define(SUBNET, 1).
-define(MIN_ADDR_BYTE, 10).
-define(MAX_ADDR_BYTE, 137).
-define(NAME_PREFIX, "rush").

-define(RUSH_IMAGE, <<"/root/images/rushling.img">>).
-define(GATOR_HOST, {192,168,0,1}).
-define(BRIDGE, <<"br1">>).

-record(st, {avail_addrs,
			 running =[],
			 total_count}).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

start_spawn(PeerIP) ->
	gen_server:call(?SERVER, {start_spawn,PeerIP}).

start_destroy(PeerIP) ->
	gen_server:call(?SERVER, {start_destroy,PeerIP}).

make_greeting() ->
	gen_server:call(?SERVER, make_greeting).

name_to_addr(Name) ->
	gen_server:call(?SERVER, {name_to_addr,Name}).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
	TC = case file:consult(?STATS_FILE) of
	{ok,Stats} ->
		proplists:get_value(total_count, Stats, 0);
	_ ->
		0
	end,

	schedule_half_life(),
	schedule_save(),

	AvailAddrs = [{192,168,?SUBNET,N}
			|| N <- lists:seq(?MIN_ADDR_BYTE, ?MAX_ADDR_BYTE)],
    {ok,#st{avail_addrs =AvailAddrs,total_count =TC}}.

handle_call({start_spawn,_PeerIP}, _From, #st{avail_addrs =[]} =St) ->
	{reply,{error,at_maximum_capacity},St};

handle_call({start_spawn,PeerIP}, _From, #st{avail_addrs =[AA|AAs],
											   running =Runs,
											   total_count =TC} =St) ->
	{_A,_B,_C,D} = AA,
	Name = list_to_binary(io_lib:format("~s~w", [?NAME_PREFIX,D])),

	spawn(fun() ->
		Extra = list_to_binary(io_lib:format("-domain ~s -ipaddr ~s "
			"-netmask 255.255.255.0 -gateway 192.168.0.1 "
			"-root /erlang -pz /rushling/ebin -pz /rushling/deps/cowboy/ebin "
			"-pz /rushling/deps/ranch/ebin -home /rushling -s rushling_app",
					[Name,inet_parse:ntoa(AA)])),

		case egator:create(Name, ?RUSH_IMAGE,
				[{memory,32},{extra,Extra},{bridge,?BRIDGE}], [{host,?GATOR_HOST}]) of

		ok ->
			rate_keeper ! spawned,

			%% {'what': 'spawned', 'name': inst_name, 'peer': peer_addr}
			Json = [{<<"what">>,<<"spawned">>},
					{<<"name">>,Name},
					{<<"peer">>,PeerIP},
					{<<"total_count">>,TC +1}],
			rush_central:announce(jsx:encode(Json));

		Error ->
			io:format("Spawning error: ~p~n", [Error])
		end
	end),

	{reply,ok,St#st{avail_addrs =AAs,
					running =[{Name,AA,PeerIP}|Runs],
					total_count =TC +1}};

handle_call({start_destroy,PeerIP}, _From, #st{running =Runs} =St) ->

	case lists:keytake(PeerIP, 3, shuffle(Runs)) of
	false ->
		{reply,{error,not_found},St};

	{value,{Name,AA,_},Runs1} ->

		do_destroy(Name, AA),

		{reply,ok,St#st{running =Runs1}}
	end;

handle_call(make_greeting, _From , #st{running =Runs,
									   total_count =TC} =St) ->
	RunInfo = [
		[{<<"name">>,Name},
		 {<<"peer">>,Peer}]
			|| {Name,_Addr,Peer} <- Runs],

	Json = [{<<"what">>,<<"greeting">>},
			{<<"running">>,RunInfo},
			{<<"total_count">>,TC}],
	{reply,jsx:encode(Json),St};

handle_call({name_to_addr,Name}, _From, #st{running =Runs} =St) ->
	case lists:keyfind(Name, 1, Runs) of
	false ->
		{reply,not_found,St};
	{_,Addr,_} ->
		{reply,Addr,St}
	end.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({address_available,AA}, #st{avail_addrs =AAs} =St) ->
	%%io:format("~s is ready for reuse~n", [inet_parse:ntoa(AA)]),
	{noreply,St#st{avail_addrs =[AA|AAs]}};

handle_info(half_life, #st{running =Runs} =St) ->
	N = length(Runs),
	Threshold = (?HALF_LIFE_MAX_PROB -?HALF_LIFE_BASE_PROB)
			*N /?MAX_CAPACITY +?HALF_LIFE_BASE_PROB,
	StarsAligned = random:uniform() < Threshold,

	schedule_half_life(),

	if StarsAligned and (N >= ?HALF_LIFE_CUT) ->
		NumDoomed = N div ?HALF_LIFE_PART,
		io:format("HL: destroy ~w domain(s)~n", [NumDoomed]),
		{Doomed,Runs1} = lists:split(NumDoomed, shuffle(Runs)),
		lists:foreach(fun({Name,AA,_}) ->
			do_destroy(Name, AA)
		end, Doomed),
		{noreply,St#st{running =Runs1}};
	true ->
		{noreply,St}
	end;

handle_info(save_stats, #st{total_count =TC} =St) ->
	{ok,F} = file:open(?STATS_FILE, [write]),
	io:format(F, "~p.", [{total_count,TC}]),
	file:close(F),
	schedule_save(),
    {noreply,St}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%------------------------------------------------------------------------------

do_destroy(Name, AA) ->
	Self = self(),
	spawn(fun() ->
	
		case egator:destroy(Name, [{host,?GATOR_HOST}]) of
		ok ->
			rate_keeper ! destroyed,
			Self ! {address_available,AA},

			%% {'what': 'destroyed', 'name': inst_name}
			Json = [{<<"what">>,<<"destroyed">>},
					{<<"name">>,Name}],
			rush_central:announce(jsx:encode(Json));

		Error ->
			io:format("Destruction error: ~p~n", [Error])
		end
	end).

cleanup_stale_rushlings() ->

	{ok,Stale} = egator:list(<<?NAME_PREFIX>>, [{host,?GATOR_HOST}]),

	if Stale =/= [] ->
		io:format("~w stale rushlings found, cleaning up...~n", [length(Stale)]);
	true ->
		ok
	end,

	Self = self(),
	lists:foreach(fun({Name,_}) ->
		spawn(fun() ->
			ok = egator:destroy(Name, [{host,?GATOR_HOST}]),
			Self ! {stale_destroyed,Name}
		end)
	end, Stale),

	wait_for_stale(Stale).

wait_for_stale([]) ->
	ok;
wait_for_stale(Stale) ->
	receive
	{stale_destroyed,Name} ->
		wait_for_stale(lists:keydelete(Name, 1, Stale))
	end.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

schedule_save() ->
	erlang:send_after(?STATS_UPDATE, self(), save_stats).

schedule_half_life() ->
	erlang:send_after(?HALF_LIFE_UPDATE, self(), half_life).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% shuffle(List1) -> List2
%% Takes a list and randomly shuffles it. Relies on random:uniform
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

shuffle([]) ->
	[];
shuffle(List) ->
%% Determine the log n portion then randomize the list.
   randomize(round(math:log(length(List)) + 0.5), List).

randomize(1, List) ->
   randomize(List);
randomize(T, List) ->
   lists:foldl(fun(_E, Acc) ->
                  randomize(Acc)
               end, randomize(List), lists:seq(1, (T - 1))).

randomize(List) ->
   D = lists:map(fun(A) ->
                    {random:uniform(), A}
             end, List),
   {_, D1} = lists:unzip(lists:keysort(1, D)), 
   D1.

%%EOF
