
-module(zergrush_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	Central = ?CHILD(rush_central, worker),
	Pool = ?CHILD(spawning_pool, worker),
	Keeper = ?CHILD(rate_keeper, worker),

    {ok, { {one_for_one, 5, 10}, [Central,Pool,Keeper]} }.

%%EOF
