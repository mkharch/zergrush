-module(instances_handler).
-export([init/3]).
-export([handle/2,terminate/3]).

-define(SPAWN_PACK_SIZE, 8).
-define(DESTROY_PACK_SIZE, 7).

init({tcp,http}, Req, []) ->
	{ok,Req,[]}.

handle(Req, St) ->
	{PeerIP,_} = cowboy_req:header(<<"x-real-ip">>, Req, <<"UNKNOWN">>),
	case cowboy_req:method(Req) of
	{<<"PUT">>,_} ->
		case spawn_instances(?SPAWN_PACK_SIZE, PeerIP) of
		ok ->
			{ok,Reply} = cowboy_req:reply(204, [], Req),
			{ok,Reply,St};
		_ ->
			{ok,Reply} = cowboy_req:reply(406, [], Req),
			{ok,Reply,St}
		end;
	{<<"DELETE">>,_} ->
		destroy_instances(?DESTROY_PACK_SIZE, PeerIP),
		{ok,Reply} = cowboy_req:reply(204, [], Req),
		{ok,Reply,St}
	end.

terminate(_What, _Req, _St) ->
	ok.

spawn_instances(0, _PeerIP) ->
	ok;
spawn_instances(N, PeerIP) ->
	case spawning_pool:start_spawn(PeerIP) of
	ok ->
		spawn_instances(N -1, PeerIP);
	Other ->
		io:format("spawn_instances: ~p~n", [Other]),
		Other
	end.

destroy_instances(0, _PeerIP) ->
	ok;
destroy_instances(N, PeerIP) ->
	case spawning_pool:start_destroy(PeerIP) of
	ok ->
		destroy_instances(N -1, PeerIP);
	Other ->
		io:format("destroy_instances: ~p~n", [Other]),
		Other
	end.

%%EOF
