-module(toppage_handler).
-export([init/3]).
-export([handle/2,terminate/3]).

-define(MAX_CONNS, 16).

init({tcp,http}, Req, _Opts) ->
	{ok,Req,[]}.

handle(Req, St) ->
	case rush_central:count_web_sockets() of
	N when N >= ?MAX_CONNS ->
		{ok,Body} = too_many_dtl:render([{max_conns,?MAX_CONNS}]),
		{ok,Reply} = cowboy_req:reply(200, [], Body, Req),
		{ok,Reply,St};
	_ ->
		{ok,Body} = rush_dtl:render([]),
		{ok,Reply} = cowboy_req:reply(200, [], Body, Req),
		{ok,Reply,St}
	end.

terminate(_What, _Req, _St) ->
	ok.

%%EOF

