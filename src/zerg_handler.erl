-module(zerg_handler).
-export([init/3]).
-export([handle/2,terminate/3]).

init({tcp,http}, Req, _Opts) ->
	{ok,Req,[]}.

handle(Req, St) ->
	{Name,_} = cowboy_req:binding(name, Req),
	Body = case spawning_pool:name_to_addr(Name) of
	not_found ->
		<<"[destroyed]">>;
	Addr ->
		Uri = "http://" ++ inet_parse:ntoa(Addr) ++ ":8000",
		case httpc:request(Uri) of
		{ok,{{_,200,_},_,ZergBody}} ->
			ZergBody;
		{error,Reason} ->
			list_to_binary(io_lib:format("<pre>Error: ~p</pre>", [Reason]))
		end
	end,

	{ok,Reply} = cowboy_req:reply(200, [], Body, Req),
	{ok,Reply,St}.

terminate(_What, _Req, _St) ->
	ok.

%%EOF

