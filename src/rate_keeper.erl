-module(rate_keeper).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

-define(BAR_COUNT, 30).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
	TS = timestamp(),
	Bars = dict:from_list([{N,{0,0}}
				|| N <- lists:seq(TS -?BAR_COUNT +1, TS)]),
	schedule_update(),
    {ok,Bars}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(spawned, Bars) ->
	TS = timestamp(),
	Bars1 = dict:update(TS, fun({C1,C2}) ->
		{C1 +1,C2}
	end, {1,0}, Bars),
	trim_bars(Bars1);

handle_info(destroyed, Bars) ->
	TS = timestamp(),
	Bars1 = dict:update(TS, fun({C1,C2}) ->
		{C1,C2 +1}
	end, {0,1}, Bars),
	trim_bars(Bars1);

handle_info(time_passes, Bars) ->
	TS = timestamp(),
	Bars1 = dict:update(TS, fun(V) -> V end, {0,0}, Bars),

	self() ! update_bars,
	trim_bars(Bars1);

handle_info(update_bars, Bars =St) ->

	Json = [{<<"what">>,<<"bars">>},
			{<<"bars">>,[
				[{<<"t">>,TS},
				 {<<"s">>,Spawned},
				 {<<"d">>,Destroyed}]
		|| {TS,{Spawned,Destroyed}} <- lists:keysort(1, dict:to_list(Bars))]}],

	rush_central:announce(jsx:encode(Json)),
	schedule_update(),
    {noreply,St}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

trim_bars(Bars) ->
	Size = dict:size(Bars),
	if Size > ?BAR_COUNT ->
		MinTS =lists:min(dict:fetch_keys(Bars)),
		{noreply,dict:erase(MinTS, Bars)};
	true ->
		{noreply,Bars}
	end.
		
schedule_update() ->
	erlang:send_after(1000, self(), time_passes).

timestamp() ->
	{Mega,Secs,_} = now(),
	Mega *1000000 +Secs.

%%EOF
