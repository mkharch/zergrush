-module(zergrush_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-export([start/0]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start() ->
	spawning_pool:cleanup_stale_rushlings(),

	inets:start(),

	application:start(crypto),
	application:start(ranch),
	application:start(cowboy),
	application:start(zergrush).

start(_StartType, _StartArgs) ->

	MimeTypes = [{<<".css">>,[<<"text/css">>]},
		{<<".js">>,[<<"application/javascript">>]},
		{<<".png">>,[<<"image/png">>]}],

	StaticHandlers = [
		{"/css/[...]",cowboy_static,[{directory,"priv/css"},
									   {mimetypes,MimeTypes}]},
	    {"/images/[...]",cowboy_static,[{directory,"priv/images"},
									   {mimetypes,MimeTypes}]},
		{"/js/[...]",cowboy_static,[{directory,"priv/js"},
											   {mimetypes,MimeTypes}]}
	],

	PageHandlers = StaticHandlers ++ [
		{"/",toppage_handler,[]},
		{"/instances",instances_handler,[]},
		{"/instances/:name",zerg_handler,[]}
	],

	Dispatch = cowboy_router:compile([
		{"rush.erlangonxen.org",PageHandlers}
	]),

	{ok,_} = cowboy:start_http(www, 100,
		[{port,1077}],
		[{env,[{dispatch,Dispatch}]}]),

	Websocket = cowboy_router:compile([
		{"rush.erlangonxen.org",[{"/feedback",feedback_handler,[]}]}
	]),

	{ok,_} = cowboy:start_http(ws, 100,
		[{port,1078}],
		[{env,[{dispatch,Websocket}]}]),

	io:format("Accepting connections...~n", []),

    zergrush_sup:start_link().

stop(_State) ->
    ok.
